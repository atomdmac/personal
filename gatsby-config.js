module.exports = {
  pathPrefix: `/personal`,
  siteMetadata: {
    title: `Adam Macumber`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
