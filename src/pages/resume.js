import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
  <div>
    <h1>Resume</h1>
    <p>This is where my resume will go soon.</p>
  </div>
)

export default SecondPage
