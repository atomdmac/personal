import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Oh hi!</h1>
    <p>It's me, Adam.  Thanks for dropping by.</p>
    <p>I don't have a ton to say at the moment but y'know... maybe that'll change in the near future.</p>
    <Link to="/resume/">Here's my resume</Link>
  </div>
)

export default IndexPage
